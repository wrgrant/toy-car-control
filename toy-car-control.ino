#include <Servo.h>


const int pwmOutPin = 9;
const int btnFwdPin = 12;
const int btnRevPin = 11;
const int speedControlPin = A0;

Servo myServo;

// Define 'stopped' speed. Using servo class, so this is 90.
int speedStopped = 90;
int currentSpeed = speedStopped;

// Define max reverse speed.
int speedRev = 9;
int speedFwd = 0;
int maxSpeedFwd;
int maxSpeedRev = speedStopped - speedRev;

int isFwd;
int isRev;


void setup() {
  Serial.begin(9600);
  
  // Setup the pins for forward / reverse.
  pinMode(btnFwdPin, INPUT_PULLUP);
  pinMode(btnRevPin, INPUT_PULLUP);

  // Setup the servo out pin.
  myServo.attach(pwmOutPin);
}


void loop() {
  // the pull-up means the pushbutton's logic is inverted.
  // It goes HIGH when it's open, and LOW when it's pressed.
  // So do a NOT while reading to make logic simpler below.
  isFwd = !digitalRead(btnFwdPin);
  isRev = !digitalRead(btnRevPin);

  // Get the user-defined max speed.
  speedFwd = map(analogRead(speedControlPin), 0, 680, 4, 30);

  // Recalculate the max speed in 'servo' coordinates.
  maxSpeedFwd = speedStopped + speedFwd;

  // Act upon forward button. Only if not pressing reverse and speed is stopped or greater.
  if (!isRev & (currentSpeed >= speedStopped)) {
    if (isFwd)
      accelerate();  // pressed
    else
      decelerate();  // not pressed
  }


  // Act upon reverse button. Only if not pressing forward and speed is stopped or less
  if (!isFwd & (currentSpeed <= speedStopped)) {
    if (isRev)
      accelerateReverse();
    else
      decelerateReverse();
  }

  Serial.print("current speed = ");
  Serial.print(currentSpeed);
  Serial.print(" max speed = ");
  Serial.print(maxSpeedFwd);
  Serial.print(" isFwd = ");
  Serial.print(isFwd);
  Serial.print(" isRev = ");
  Serial.println(isRev);

  myServo.write(currentSpeed);
  delay(75);
}


void accelerate() {
  // Increase forward speed.
  // Initial case for stopped. Give big jump to make motion happen immediately.
  if (currentSpeed == speedStopped)
    currentSpeed += 2;
  // Normal case.
  else if (currentSpeed < maxSpeedFwd)
    currentSpeed += 1;
  else
    // Ensure it doesn't go above max.
    currentSpeed = maxSpeedFwd;
}


void decelerate() {
  // Decrease forward speed.
  if (currentSpeed > speedStopped)
    currentSpeed -= 1;
  else
    // Ensure it stops at speedStopped.
    currentSpeed = speedStopped;
}


void accelerateReverse() {
  // Increase reverse speed.
  // Initial case dor stopped. Give big jump to make motion happen immediately.
  if (currentSpeed == speedStopped)
    currentSpeed -= 2;
  else if (currentSpeed > maxSpeedRev)
    currentSpeed -= 1;
  else
    // Ensure it doesn't go below speedRev
    currentSpeed = maxSpeedRev;
}


void decelerateReverse() {
  // Decrease reverse speed.
  if (currentSpeed < speedStopped)
    currentSpeed += 1;
  else
    // Ensure it stops at speedStopped.
    currentSpeed = speedStopped;
}
